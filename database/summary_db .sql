-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Янв 23 2022 г., 09:42
-- Версия сервера: 5.5.62
-- Версия PHP: 7.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `summary_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `company`
--

CREATE TABLE `company` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `website` varchar(50) CHARACTER SET utf8 NOT NULL,
  `address` varchar(100) CHARACTER SET utf8 NOT NULL,
  `phone_number` varchar(15) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `company`
--

INSERT INTO `company` (`id`, `name`, `website`, `address`, `phone_number`) VALUES
(1, 'ТОВ ІСМ Україна', 'http://www.ism-ukraine.com/', 'провулок 1-й Капітульний, 20, Житомир, Житомирська область, 10002', '0412 445 403'),
(2, 'Вадіус', 'http://vadyus.com.ua/', 'вулиця Кафедральна, 8, Житомир, Житомирська область, 10000', '093 408 5548'),
(3, 'Olimp Digital', 'https://olimpdigital.com/', 'вулиця Мала Бердичівська, 17б, Житомир, Житомирська область, 10014', '093 121 4921'),
(4, 'DF-Studio', 'https://www.dfstudio.com/', 'буд. 184, офіc 308, проспект Незалежності, Житомир, Житомирська область, 10002', '093 109 0914');

-- --------------------------------------------------------

--
-- Структура таблицы `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20220118192606', '2022-01-18 22:38:32', 269),
('DoctrineMigrations\\Version20220118195222', '2022-01-18 22:54:35', 468);

-- --------------------------------------------------------

--
-- Структура таблицы `summary`
--

CREATE TABLE `summary` (
  `id` int(10) UNSIGNED NOT NULL,
  `vacancy` varchar(50) CHARACTER SET utf8 NOT NULL,
  `description` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `published` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `summary`
--

INSERT INTO `summary` (`id`, `vacancy`, `description`, `published`, `created_at`, `updated_at`, `user_id`) VALUES
(1, 'Middle PHP Developer', 'Перевірка резюме', 0, '2022-01-20 17:50:33', '2022-01-20 17:50:33', 3),
(2, 'Front End (Angular) Developer', 'Перевірка резюме', 1, '2022-01-20 17:54:07', '2022-01-20 17:54:07', 3),
(3, 'Java Developer', 'Опис третього резюме', 1, '2022-01-20 18:08:33', '2022-01-20 20:17:41', 3),
(4, 'Javascript Developer', 'Опис четвертого резюме', 1, '2022-01-20 18:10:37', '2022-01-20 18:10:37', 3),
(5, '.NET Middle Developer', 'Опис п\'ятого резюме', 1, '2022-01-20 19:33:36', '2022-01-20 19:49:09', 3),
(7, 'Senior Backend Developer', 'Senior Backend Developer Description', 1, '2022-01-20 20:04:27', '2022-01-20 20:04:27', 2),
(8, 'Нова посада', 'Нове резюме', 1, '2022-01-21 10:18:29', '2022-01-21 10:18:29', 2),
(9, 'Go Developer', 'Some text', 0, '2022-01-21 22:40:12', '2022-01-21 22:40:12', 2),
(10, 'Супер нова посада', 'Опис супер посади', 1, '2022-01-22 17:36:34', '2022-01-22 17:36:34', 2),
(11, 'C# Developer', 'It\'s C#', 1, '2022-01-22 20:43:37', '2022-01-22 20:43:37', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `summary_company`
--

CREATE TABLE `summary_company` (
  `id` int(10) UNSIGNED NOT NULL,
  `summary_id` int(10) UNSIGNED NOT NULL,
  `company_id` int(10) UNSIGNED NOT NULL,
  `received_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `summary_company`
--

INSERT INTO `summary_company` (`id`, `summary_id`, `company_id`, `received_at`) VALUES
(1, 7, 4, '2022-01-21 15:26:20');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_verified` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `email`, `roles`, `password`, `is_verified`) VALUES
(1, 'yuriipazl@gmail.com', '[\"ROLE_USER\"]', '$2y$13$G78GWO8BVhruYB2mQpfQsua.wG7IfynJNS4vp9U0A4Hr0XnGNshLO', 0),
(2, 'mark@gmail.com', '[\"ROLE_USER\"]', '$2y$13$PXzzHGSrK1nT5.sizhaIrefc.5816nNcyDHeT648mQDnHrTqXjhb2', 0),
(3, 'camille@gmail.com', '[\"ROLE_USER\"]', '$2y$13$NJkrxh4WzhEU5MP0xccRJuW6xz8m43AkclWZdnL3jmYtARHcJQyLC', 0),
(4, 'john@gmail.com', '[\"ROLE_USER\"]', '$2y$13$cP0Yl0tI.dWC0vSd3lAyueXZjM2pHxNEjsIHULIZ3SutE3wx7qAby', 0),
(5, 'kentavr@gmail.com', '[\"ROLE_USER\"]', '$2y$13$HjCXiSadFNuiHLfpOE5kBuNl.XnmJ/s3C5FhnynqF/Swz95ubvt6K', 0),
(6, 'nicole@gmail.com', '[\"ROLE_USER\"]', '$2y$13$NvMVTNgTmdOIpcdghf0b.uYT3dNEcGWFu4Luw57yYVFf9Z3uGqBPi', 0),
(7, 'alex@gmail.com', '[\"ROLE_USER\"]', '$2y$13$W9.r7fY4LVkkNA34fwgBl.BqXVqYNYOUlIDo6QvPNucz2hZqIjlvW', 0);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone_number` (`phone_number`);

--
-- Индексы таблицы `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `summary`
--
ALTER TABLE `summary`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `summary_company`
--
ALTER TABLE `summary_company`
  ADD PRIMARY KEY (`id`),
  ADD KEY `summary_id` (`summary_id`),
  ADD KEY `company_id` (`company_id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `company`
--
ALTER TABLE `company`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `summary`
--
ALTER TABLE `summary`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `summary_company`
--
ALTER TABLE `summary_company`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `summary`
--
ALTER TABLE `summary`
  ADD CONSTRAINT `summary_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `summary_company`
--
ALTER TABLE `summary_company`
  ADD CONSTRAINT `summary_company_ibfk_1` FOREIGN KEY (`summary_id`) REFERENCES `summary` (`id`),
  ADD CONSTRAINT `summary_company_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
