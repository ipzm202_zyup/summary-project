<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\CompanyType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\CompanyRepository;
use App\Entity\SummaryCompany;
use App\Entity\Company;
use App\Entity\Summary;

/**
* @Route("/company", name="company.")
*/
class CompanyController extends AbstractController
{
    /**
     * @Route("/index", name="index")
     * @param CompanyRepository $companyRepository
     * @return Response
     */
    public function index(CompanyRepository $companyRepository): Response
    {
        $companies = $companyRepository->findAll();
        return $this->render('company/index.html.twig', [
            'controller_name' => 'CompanyController',
            'companies' => $companies,
        ]);
    }

    /**
     * @Route("/create", name="create", methods={"GET","POST"})
     * @param Request $request 
     * @return Response
     */
    public function create(Request $request): Response 
    {
        $company = new Company();

        $form = $this->createForm(CompanyType::class, $company);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager= $this->getDoctrine()->getManager();
            $entityManager->persist($company);
            $entityManager->flush();

            $this->addFlash('success', 'Компанія успішно додана!');

            return $this->redirectToRoute('company.index');
        }

        return $this->render('company/create.html.twig', [
            'company' => $company,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id}", name="edit", methods={"GET","POST"})
     * @param Request $request 
     * @return Response
     */
    public function edit(Request $request, Company $company): Response
    {
        $form = $this->createForm(CompanyType::class, $company);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Компанія успішно змінена!');

            return $this->redirectToRoute('company.index');
        }

        return $this->render('company/edit.html.twig', [
            'company' => $company,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/show/{id}", name="show", methods={"GET"})
     * @param Company $company
     * @param Request $request 
     * @return Response 
     */
    public function show(Request $request, Company $company, Summary $summary): Response 
    {      
        $companySummaries = $this->getDoctrine()->getManager()
        ->createQueryBuilder()
        ->select('sc, s.vacancy, s.createdAt, s.description')
        ->from(SummaryCompany::class, 'sc')
        ->innerJoin(Company::class, 'c', 'with', 'c.id = sc.companyId')
        ->innerJoin(Summary::class, 's', 'with', 's.id = sc.summaryId')
        ->where('c.id = :company_id')
        ->orderBy('sc.receivedAt', 'DESC')
        ->setParameter('company_id', $company->getId())
        ->getQuery()
        ->getResult();
        
        //dd($companySummaries);

        return $this->render('company/show.html.twig', [
            'companySummaries' => $companySummaries,
            'company' => $company,
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete")
     * @param Company $company 
     */
    public function remove(Company $company)
    {
        $entityManager= $this->getDoctrine()->getManager();
        $entityManager->remove($company);
        $entityManager->flush();
        
        $this->addFlash('success', 'Компанію успішно видалено!');

        return $this->redirect($this->generateUrl('company.index'));
    }
}
