<?php

namespace App\Controller;

use App\Form\Type\ChangePasswordType;
use App\Form\UserType;
use App\Entity\Article;
use App\Service\UploaderHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Controller used to manage current user.
 *
 * @Route("/profile", name="profile.")
 * @IsGranted("ROLE_USER")
 *
 */
class ProfileController extends AbstractController
{
    /**
     * @Route("/index", name="index")
     */
    public function index(Request $request ): Response
    {
        $user = $this->getUser();

        return $this->render('profile/index.html.twig', [
            'user' => $user,
        ]);
    }

     /**
     * @Route("/highcharts", name="highcharts")
     */
    public function highcharts(Request $request ): Response
    {
        return $this->render('profile/highcharts.html.twig');
    }

}