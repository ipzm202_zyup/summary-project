<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\SummaryType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\SummaryRepository;
use App\Entity\Summary;
use App\Entity\Company;
use App\Entity\SummaryCompany;
use App\Form\SummaryCompanyType;
use Symfony\Component\Security\Core\Security;

/**
* @Route("/summary", name="summary.")
*/
class SummaryController extends AbstractController
{

    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {
       $this->security = $security;
    }

    /**
     * @Route("/index", name="index")
     * @param SummaryRepository $summaryRepository
     * @return Response
     */
    public function index(SummaryRepository $summaryRepository): Response
    {
        $summaries= $summaryRepository->findAll();
        return $this->render('summary/index.html.twig', [
            'controller_name' => 'SummaryController',
            'summaries' => $summaries,
        ]);
    }

    /**
     * @Route("/create", name="create", methods={"GET","POST"})
     * @param Request $request 
     * @return Response
     */
    public function create(Request $request): Response 
    {
        $summary = new Summary();
        $form = $this->createForm(SummaryType::class, $summary);
        $summary->setUser($this->getUser());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager= $this->getDoctrine()->getManager();
            $entityManager->persist($summary); 
            $entityManager->flush();

            $this->addFlash('success', 'Резюме успішно створено!');

            return $this->redirectToRoute('summary.index');
        }

        return $this->render('summary/create.html.twig', [
            'summary' => $summary,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/send_summary", name="send_summary", methods={"GET","POST"})
     * @param Request $request 
     * @return Response
     */
    public function send_summary(Request $request): Response
    {
        $summaryCompany = new SummaryCompany();
        
        /*
        $em = $this->getDoctrine()->getManager();
        $summary = $em->getRepository(Summary::class)->find($id);
        $company = $em->getRepository(Company::class)->find($id);
        
        $summaryCompanyRepository = $em->getRepository(SummaryCompany::class);
        */
        
        //$summaryCompany->setSummaries(new Summary());
        //$summaryCompany->setCompanies(new Company());
        //dd($summaryCompany);
        
        //$form = $this->createForm(SummaryCompanyType::class, $summaryCompany);
        
        $form = $this->createForm(SummaryCompanyType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) { 

            $entityManager= $this->getDoctrine()->getManager();
            $entityManager->persist($summaryCompany); 
            dd($summaryCompany);
            $entityManager->flush();

            $this->addFlash('success', 'Резюме успішно відправлено!');

            return $this->redirectToRoute('summary.index');

            /*
            $data = $form->getData();

            if( !$summaryCompanyRepository->findBy( [
                'company'=>$request->request->get('summary_company')['company'],
                'summary'=>$request->request->get('summary_company')['summary'] ])) 
                {    
                $entityManager= $this->getDoctrine()->getManager();
                $entityManager->persist($data); 
                //dd($summaryCompany);
                $entityManager->flush();

                $this->addFlash('success', 'Резюме успішно відправлено!');

                return $this->redirectToRoute('summary.index');
            }
            */
        }
        
        return $this->render('summary/send_summary.html.twig', [
            //'summary' => $summary,
            //'company' => $company,
            //'companies'=>$summaryCompanyRepository->findBy(['company' => $company->getId()]),
            //'summaries'=>$summaryCompanyRepository->findBy(['summary' => $summary->getId()]),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id}", name="edit", methods={"GET","POST"})
     * @param Request $request 
     * @return Response
     */
    public function edit(Request $request, Summary $summary): Response
    {
        $form = $this->createForm(SummaryType::class, $summary);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $summary->setUpdatedAt(new \DateTime());
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Резюме успішно змінено!');

            return $this->redirectToRoute('summary.index');
        }

        return $this->render('summary/edit.html.twig', [
            'summary' => $summary,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/show/{id}", name="show", methods={"GET"})
     * @param Summary $summary
     * @return Response 
     */
    public function show(Summary $summary): Response 
    {
        return $this->render('summary/show.html.twig', [
            'summary' => $summary,
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete")
     * @param Summary $summary 
     */
    public function remove(Summary $summary)
    {
        $entityManager= $this->getDoctrine()->getManager();
        $entityManager->remove($summary);
        $entityManager->flush();
        
        $this->addFlash('success', 'Резюме успішно видалено!');

        return $this->redirect($this->generateUrl('summary.index'));
    }
}
