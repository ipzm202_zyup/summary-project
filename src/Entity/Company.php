<?php

namespace App\Entity;

use App\Repository\CompanyRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CompanyRepository::class)
 */
class Company
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $website;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $phone_number;

     /**
     * @ORM\OneToMany(targetEntity="App\Entity\SummaryCompany", mappedBy="companies")
     */
    protected $studentCompanies;

    public function __construct()
    {
        $this->studentCompanies = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phone_number;
    }

    public function setPhoneNumber(string $phone_number): self
    {
        $this->phone_number = $phone_number;

        return $this;
    }

    /**
     * @return Collection|SummaryCompany[]|null
     */
    public function getSummaryCompanies(): Collection
    {
        return $this->summaryCompanies;
    }

    public function addSummaryCompany(SummaryCompany $summaryCompany): self
    {
        if (!$this->summaryCompanies->contains($summaryCompany)) {
            $this->summaryCompanies[] = $summaryCompany;
            $SummaryCompany->setCompany($this);
        }

        return $this;
    }

    public function removeSummaryCompany(SummaryCompany $summaryCompany): self
    {
        if ($this->summaryCompanies->contains($summaryCompany)) {
            $this->summaryCompanies->removeElement($summaryCompany);
            
            if ($summaryCompany->getCompany() === $this) {
                $summaryCompany->setCompany(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
