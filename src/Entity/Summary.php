<?php

namespace App\Entity;

use App\Repository\SummaryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=SummaryRepository::class)
 */
class Summary
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $vacancy;

    /**
     * @ORM\Column(type="string", length=1000)
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

     /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="summaries")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="boolean")
     */
    private $published;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SummaryCompany", mappedBy="summaries")
     */
    protected $summaryCompanies;

    public function __construct()
    {
        $this->summaryCompanies = new \Doctrine\Common\Collections\ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVacancy(): ?string
    {
        return $this->vacancy;
    }

    public function setVacancy(string $vacancy): self
    {
        $this->vacancy = $vacancy;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getUserId(): ?User
    {
        return $this->user_id;
    }

    public function getPublished(): ?bool
    {
        return $this->published;
    }

    public function setPublished(bool $published): self
    {
        $this->published = $published;

        return $this;
    }

     /**
     * @return Collection|SummaryCompany[]|null
     */
    public function getSummaryCompanies(): Collection
    {
        return $this->summaryCompanies;
    }

    public function addSummaryCompany(SummaryCompany $summaryCompany): self
    {
        if (!$this->$summaryCompanies->contains($summaryCompany)) {
            $this->$summaryCompanies[] = $summaryCompany;
            $summaryCompany->setSummary($this);
        }

        return $this;
    }

    public function removeSummaryCompany(SummaryCompany $summaryCompany): self
    {
        if ($this->$summaryCompanies->contains($summaryCompany)) {
            $this->$summaryCompanies->removeElement($summaryCompany);
          
            if ($summaryCompany->getSummary() === $this) {
                $summaryCompany->setSummary(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getVacancy();
    }

}
