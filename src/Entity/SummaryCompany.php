<?php

namespace App\Entity;

use App\Repository\SummaryCompanyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=SummaryCompanyRepository::class)
 */
class SummaryCompany
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

     /**
     * @var int
     *
     * @ORM\Column(name="company_id", type="integer")
     */
    private $companyId;

     /**
     * @var int
     *
     * @ORM\Column(name="summary_id", type="integer")
     */
    private $summaryId;

     /**
     * @ORM\Column(type="datetime")
     */
    private $receivedAt;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Company", inversedBy="summaryCompanies")
    * @ORM\JoinColumn(name="company_id", referencedColumnName="id", nullable=true)
     */
    private $companies;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Summary", inversedBy="summaryCompanies")
     * @ORM\JoinColumn(name="summary_id", referencedColumnName="id", nullable=true)
     */
    private $summaries;

    public function __construct()
    {
        //$this->companies = new \Doctrine\Common\Collections\ArrayCollection();
        //$this->summaries = new \Doctrine\Common\Collections\ArrayCollection();
        $this->receivedAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }
   
    public function getReceivedAt(): ?\DateTimeInterface
    {
        return $this->receivedAt;
    }

    public function setReceivedAt(\DateTimeInterface $receivedAt): self
    {
        $this->receivedAt = $receivedAt;

        return $this;
    }

    
    public function getSummaries(): ?Summary
    {
        return $this->summaries;
    }

    public function setSummaries(?Summary $summaries): self
    {
        $this->summaries = $summaries;

        return $this;
    }

    public function getCompanies(): ?Company
    {
        return $this->companies;
    }

    public function setCompanies(?Company $companies): self
    {
        $this->companies = $companies;

        return $this;
    }
}