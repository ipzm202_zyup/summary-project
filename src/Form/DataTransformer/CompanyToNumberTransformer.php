<?php
// src/Form/DataTransformer/CompanyToNumberTransformer.php
namespace App\Form\DataTransformer;

use App\Entity\Company;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class CompanyToNumberTransformer implements DataTransformerInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Transforms an object (Company) to a string (number).
     *
     * @param  Company|null $course
     * @return string
     */
    public function transform($company)
    {
        if (null === $company) {
            return '';
        }

        return $company->getId();
    }

    /**
     * Transforms a string (number) to an object (Company).
     *
     * @param  string $company_id
     * @return Company|null
     * @throws TransformationFailedException if object (Company) is not found.
     */
    public function reverseTransform($company_id)
    {
        $company = $this->entityManager
            ->getRepository(Company::class)
            // query for the course with this id
            ->find($company_id)
        ;

        if (null === $company) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf(
                'A Company with Id "%s" does not exist!',
                $company_id
            ));
        }

        return $company;
    }
}