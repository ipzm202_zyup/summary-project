<?php
// src/Form/DataTransformer/SummaryToNumberTransformer.php
namespace App\Form\DataTransformer;

use App\Entity\Summary;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class SummaryToNumberTransformer implements DataTransformerInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Transforms an object (Summary) to a string (number).
     *
     * @param  Summary|null $summary
     * @return string
     */
    public function transform($summary)
    {
        if (null === $summary) {
            return '';
        }

        return $summary->getId();
    }

    /**
     * Transforms a string (number) to an object (Summary).
     *
     * @param  string sSummary_id
     * @return Summary|null
     * @throws TransformationFailedException if object (Summary) is not found.
     */
    public function reverseTransform($summary_id)
    {
        $summary = $this->entityManager
            ->getRepository(Summary::class)
            // query for the Summary with this id
            ->find($summary_id)
        ;

        if (null === $student) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf(
                'A Summary with Id "%s" does not exist!',
                $summary_id
            ));
        }

        return $summary;
    }
}