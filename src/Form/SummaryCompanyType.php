<?php

namespace App\Form;

use App\Entity\SummaryCompany;
use App\Entity\Company;
use App\Entity\Summary;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\ChoiceList\ChoiceList;
use App\Form\DataTransformer\SummaryToNumberTransformer;
use App\Form\DataTransformer\CompanyToNumberTransformer;

class SummaryCompanyType extends AbstractType
{
    private $summaryTransformer;
    private $companyTransformer;

    public function __construct(SummaryToNumberTransformer $summaryTransformer, CompanyToNumberTransformer $companyTransformer)
    {
        $this->summaryTransformer = $summaryTransformer;
        $this->companyTransformer = $companyTransformer;
    }
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
            $builder
            ->add('summaries', EntityType::class, [
                'class' => Summary::class,
                'placeholder' => 'Виберіть посаду',
                'choice_label' => 'vacancy'
            ])
            ->add('companies', EntityType::class, [
                'class' => Company::class,
                'placeholder' => 'Виберіть компанію',
                'choice_label' => 'name'
            ])
        ;

        //$builder->get('summaries')->addModelTransformer($this->summaryTransformer);
        //$builder->get('companies')->addModelTransformer($this->companyTransformer);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SummaryCompany::class,
        ]);
    }
}